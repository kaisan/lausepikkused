//lisa siia addeventlistener
var getTextArea = document.getElementById("textarea");
var getHighLights = document.getElementById("highlights");
var getInput = document.getElementById("counter");
getInput.value = 0;

getTextArea.addEventListener("input", findText);
getTextArea.addEventListener("scroll", scrollAlong)

// funktsioon, selleks, et leida arrayst unikaalsed väärtused
function onlyUnique(value, index, self) { 
  return self.indexOf(value) === index;
}

// funktsioon, kus loetakse textarea sisu ja kirjutatakse see highlihterisse
function findText() {
  var value = getTextArea.value
  var laused = value.split(/[\.?!]+/);
  var laused2 = laused.filter(onlyUnique);
  var lause;
  var sonad;
  let counter = 0;

  //arvesta unikaalseid lauseid ja lisa igale unikaalsele lausele span element
  for (lause in laused2){
    sonad = laused2[lause].split(' ');
    sonad = sonad.filter(n => n);

    if (sonad.length >= 15 && lause % 2 === 0){
      var re = new RegExp(laused2[lause], 'g');
      value = value.replace(re, '<span id="span1">'+laused2[lause]+'</span>');
    }
    else if (sonad.length >= 15 && lause % 2 != 0){
      var re = new RegExp(laused2[lause], 'g');
      value = value.replace(re, '<span id="span2">'+laused2[lause]+'</span>');
    }    
  }
  //lisa üles ka lausete kokkuarvestaja
  for (lause in laused){
    sonad = laused[lause].split(' ');
    sonad = sonad.filter(n => n);

    if (sonad.length >= 15){
      counter += 1;
    }
  }
  // kontrolli, et ta kopeeriks teksti lõppu ka tühja rea v tühiku kui seal alguses oli
  if (value[value.length-1] === '\n') {
    value += " "
  }
  getHighLights.innerHTML = value;
  getInput.value = counter;
}

//kontrolli, et mõlemad kastid keriksid sünkroonis
function scrollAlong() {
  var scrollTop = getTextArea.scrollTop;
  getHighLights.scrollTop = scrollTop;
}
